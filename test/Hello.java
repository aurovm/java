package test;

public class Hello {
    int number;
    String name = "John Doe";

    public Hello (String name) {
        this.name = name;
    }

    public void sayHi (String msg) {
        auro.system.println(this.name);
        auro.system.println(msg);
    }

    public static void main (/*String[] args*/) {
        Hello hello = new Hello("Jose Garcia");
        hello.sayHi("but first...");
        hello = new Hello("Gregorio Sanchez");
        hello.sayHi("good soup");
    }
}