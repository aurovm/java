package org.auro.java.semantic;

public interface RegisterSymbol extends Symbol {
    public int getRegister ();
}