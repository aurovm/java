package org.auro.java.semantic;

public class LocalFieldSymbol implements Symbol {
    public FieldDeclarationInfo info;

    public LocalFieldSymbol (FieldDeclarationInfo info) {
        this.info = info;
    }
    
    public Symbol getSymbol (String name) { return null; }
}