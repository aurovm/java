package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.Constant;
import org.auro.java.auro.Instruction;

public class ResolvedLiteralExpression implements ResolvedExpression {
    public Object value;
    public ResolvedLiteralExpression (Object v) { value = v; }

    @Override
    public int compile (MethodDeclarationInfo info, CompiledMethod method) {
        Constant cns = method.file.auroModule.addConstant(value);
        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = cns;

        method.fn.code.add(inst);

        return method.regCount++;
    }

    public ResolvedType getType () {
        if (value instanceof Integer) {
            return new PrimitiveTypeSymbol("int");
        } else {
            return null;
        }
    }
}