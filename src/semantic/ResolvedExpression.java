package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;

public interface ResolvedExpression {
    public int compile (MethodDeclarationInfo info, CompiledMethod method);
    public ResolvedType getType ();
}