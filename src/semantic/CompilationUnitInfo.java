package org.auro.java.semantic;

import org.auro.java.node.*;
import org.auro.java.compile.*;
import java.util.ArrayList;

public class CompilationUnitInfo {
    GlobalInfo globalInfo;
    CompilationUnit node;
    ArrayList<TypeDeclarationInfo> types = new ArrayList();
    SymbolTable symbols = new SymbolTable();

    public CompilationUnitInfo (CompilationUnit node, GlobalInfo globalInfo) {
        this.globalInfo = globalInfo;
        this.node = node;

        symbols.setParent(globalInfo.symbols);
    }

    public void process () {
        for (TypeDeclaration node : this.node.types) {
            TypeDeclarationInfo info = new TypeDeclarationInfo(node, this);
            //symbols.set(tpnode.name, ...);

            types.add(info);
        }

        for (TypeDeclarationInfo info : this.types) {
            info.process();
        }
    }

    public CompiledFile[] compile () {
        CompiledFile[] files = new CompiledFile[types.size()];
        for (int i = 0; i < types.size(); i++) {
            TypeDeclarationInfo info = types.get(i);

            files[i] = info.compile();
        }

        return files;
    }
}