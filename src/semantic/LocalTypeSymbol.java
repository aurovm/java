package org.auro.java.semantic;

import org.auro.java.compile.CompiledFile;
import org.auro.java.auro.Type;

public class LocalTypeSymbol implements ResolvedType {
    public TypeDeclarationInfo info;

    public LocalTypeSymbol (TypeDeclarationInfo info) {
        this.info = info;
    }

    @Override
    public Symbol getSymbol (String name) {
        FieldDeclarationInfo field = info.fieldsByName.get(name);
        if (field != null) {
            return new LocalFieldSymbol(field);
        }

        MethodDeclarationInfo method = info.methodsByName.get(name);
        if (method != null) {
            return new LocalMethodSymbol(method);
        }

        throw new Error("Symbol not found: " + name);
    }

    public Type compile (CompiledFile file) { return info.compiled.rawType; }
    public ResolvedExpression defaultValue () { return null; }
}