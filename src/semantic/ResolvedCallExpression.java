package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.Function;
import org.auro.java.auro.Instruction;

public class ResolvedCallExpression implements ResolvedExpression {
    public Symbol method;
    public ResolvedExpression instance;
    public ResolvedExpression[] arguments;

    public ResolvedCallExpression (Symbol method, ResolvedExpression instance, ResolvedExpression[] arguments) {
        this.method = method;
        this.instance = instance;
        this.arguments = arguments;
    }

    @Override
    public int compile (MethodDeclarationInfo info, CompiledMethod method) {
        Function fn;
        if (this.method instanceof AuroFunctionSymbol) {
            fn = ((AuroFunctionSymbol) this.method).fn;
            fn = method.file.importFunction(fn);
        } else if (this.method instanceof LocalMethodSymbol) {
            MethodDeclarationInfo fnInfo = ((LocalMethodSymbol) this.method).info;
            if (info.typeInfo != fnInfo.typeInfo) {
                throw new Error("Method Symbol was not declared in the same type declaration");
            }
            fn = fnInfo.compiled.fn;
        } else if (this.method instanceof ConstructorSymbol) {
            TypeDeclarationInfo tpInfo = ((ConstructorSymbol) this.method).info;

            if (tpInfo.constructor != null) {
                fn = tpInfo.constructor.compiled.fn;
            } else {
                fn = tpInfo.compiled.constructor.fn;
            }
        } else {
            throw new Error("Unknown funcion symbol: " + this.method);
        }

        int thisConst = (instance == null) ? 0 : 1;

        if (fn.ins.length != arguments.length + thisConst) {
            throw new Error("Argument count mismatch. Expression has " + arguments.length + " but function accepts " + (fn.ins.length - thisConst));
        }

        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = fn;
        inst.args = new int[fn.ins.length];

        if (instance != null) {
            inst.args[0] = instance.compile(info, method);
        }
        for (int i = 0; i < arguments.length; i++) {
            inst.args[i + thisConst] = arguments[i].compile(info, method);
        }

        method.fn.code.add(inst);

        int reg = method.regCount;
        method.regCount += fn.outs.length;
        return reg;
    }

    public ResolvedType getType () { throw new Error("Unimplemented"); }
}