package org.auro.java.semantic;

import java.util.Map;

import org.auro.java.auro.ModuleList;
import org.auro.java.auro.ModuleNode;

public class GlobalInfo {
    ModuleList auroModuleList;

    SymbolTable symbols = new SymbolTable();

    public GlobalInfo (ModuleList aml) {
        this.auroModuleList = aml;
    }

    public void createModuleSymbols () {
        for (Map.Entry<String, ModuleNode> entry : auroModuleList.root.subnodes.entrySet()) {
            symbols.set(entry.getKey(), new PackageSymbol(entry.getValue()));
        }
    }

    public void createPrimitiveSymbols () {
        for (String name : new String[]{ "int", "String" }) {   
            symbols.set(name, new PrimitiveTypeSymbol(name));
        }
    }
}