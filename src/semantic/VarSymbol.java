package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;

public class VarSymbol implements Symbol, RegisterSymbol, ResolvedExpression {
    public ResolvedType type;
    public String name;

    public int register;

    public VarSymbol (ResolvedType type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public Symbol getSymbol (String name) { return null; }
    public int getRegister () { return register; }

    public int compile (MethodDeclarationInfo info, CompiledMethod method) { return register; }
    public ResolvedType getType () { return type; };
}