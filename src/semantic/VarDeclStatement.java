package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;

public class VarDeclStatement extends ResolvedStatement {
    public VarSymbol sym;
    public ResolvedExpression expr;

    public VarDeclStatement (VarSymbol sym, ResolvedExpression expr) {
        this.sym = sym;
        this.expr = expr;
    }

    public void compile (MethodDeclarationInfo info, CompiledMethod method) {
        int reg = expr.compile(info, method);
        if (method.variables.containsKey(sym)) {
            // TODO: Add move instruction
        } else {
            method.variables.put(sym, reg);
            sym.register = reg;
        }
    }
}