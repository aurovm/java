package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.Constant;
import org.auro.java.auro.Instruction;

public class ResolvedExpressionStatement extends ResolvedStatement {
    public ResolvedExpression expr;

    public ResolvedExpressionStatement (ResolvedExpression expr) {
        this.expr = expr;
    }

    @Override
    public void compile (MethodDeclarationInfo info, CompiledMethod method) {
        this.expr.compile(info, method);
    }
}