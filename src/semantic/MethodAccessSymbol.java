package org.auro.java.semantic;

public class MethodAccessSymbol implements Symbol {
    public MethodDeclarationInfo info;
    public ResolvedExpression instance;

    public MethodAccessSymbol (MethodDeclarationInfo info, ResolvedExpression instance) {
        this.info = info;
        this.instance = instance;
    }
    
    public Symbol getSymbol (String name) { return null; }
}