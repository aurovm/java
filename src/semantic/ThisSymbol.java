package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;

public class ThisSymbol implements Symbol, ResolvedExpression {
    public MethodDeclarationInfo info;
    public int register;

    public ThisSymbol (MethodDeclarationInfo info) {
        this.info = info;
        this.register = 0;
    }
    
    public Symbol getSymbol (String name) { return null; }

    public int compile (MethodDeclarationInfo info, CompiledMethod method) { return register; }
    public ResolvedType getType () { return new LocalTypeSymbol(info.typeInfo); };
}