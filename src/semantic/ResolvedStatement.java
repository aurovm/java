package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;

public abstract class ResolvedStatement {
    public abstract void compile (MethodDeclarationInfo info, CompiledMethod method);
}