package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;

public class ParameterSymbol implements Symbol, RegisterSymbol, ResolvedExpression {
    public MethodDeclarationInfo.Parameter info;

    public ParameterSymbol (MethodDeclarationInfo.Parameter info) {
        this.info = info;
    }
    
    public Symbol getSymbol (String name) { return null; }
    public int getRegister () { return this.info.register; }

    public int compile (MethodDeclarationInfo info, CompiledMethod method) { return getRegister(); }
    public ResolvedType getType () { return info.type; };
}