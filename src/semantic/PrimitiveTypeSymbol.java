package org.auro.java.semantic;

import org.auro.java.node.Expression;
import org.auro.java.auro.Type;
import org.auro.java.compile.CompiledFile;

public class PrimitiveTypeSymbol implements ResolvedType {
    public String name;

    public PrimitiveTypeSymbol (String name) {
        this.name = name;
    }
    
    public Symbol getSymbol (String name) { return null; }

    public Type compile (CompiledFile file) {
        switch (name) {
            case "int": {
                return file.importType("auro\u001fint", "int");
            }
            case "String": {
                return file.importType("auro\u001fstring", "string");
            }
            default: {
                throw new Error("Unknown primitive: " + name);
            }
        }
    }

    public ResolvedExpression defaultValue () {
        switch (name) {
            case "int": {
                return new ResolvedLiteralExpression(0);
            }
            case "String": {
                return new ResolvedStringExpression("");
            }
            default: {
                throw new Error("Unknown primitive: " + name);
            }
        }
    }
}