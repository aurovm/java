package org.auro.java.semantic;

import org.auro.java.node.*;
import org.auro.java.compile.*;
import org.auro.java.auro.*;
import java.util.ArrayList;

public class ConstructorInfo extends MethodDeclarationInfo {
    int thisReg;

    public ConstructorInfo (MethodDeclaration node, TypeDeclarationInfo typeInfo) {
        super(node, typeInfo);
        this.onInstance = false;
    }

    public void process () {
        symbols.set("this", new ThisSymbol(this));
        super.process();
    }

    public void compileStart (CompiledFile file) {
        super.compileStart(file);
        compiled.fn.outs = new Type[]{ typeInfo.compiled.rawType };

        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = typeInfo.compiled.constructor.fn;
        inst.args = new int[0];

        compiled.fn.code.add(inst);

        thisReg = compiled.regCount;
        compiled.regCount += 1;

        ((ThisSymbol) symbols.get("this")).register = thisReg;
    }

    public void compileEnd () {
        super.compileEnd();

        Instruction end = new Instruction();
        end.type = Instruction.Type.END;
        end.args = new int[]{ thisReg };
        compiled.fn.code.add(end);
    }
}