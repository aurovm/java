package org.auro.java.semantic;

public interface Symbol {
    public Symbol getSymbol (String name);
}