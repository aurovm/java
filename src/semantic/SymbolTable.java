package org.auro.java.semantic;

import java.util.HashMap;

public class SymbolTable {
    private SymbolTable parent;
    private HashMap<String, Symbol> symbols = new HashMap();

    public SymbolTable () {
    }

    public void setParent (SymbolTable p) {
        this.parent = p;
    }

    public Symbol get (String name) {
        Symbol sym = symbols.get(name);
        if (sym == null && this.parent != null) {
            sym = this.parent.get(name);
        }

        return sym;
    }

    public void set (String name, Symbol val) {
        symbols.put(name, val);
    }

}