package org.auro.java.semantic;

import org.auro.java.node.*;

class ResolveVisitor implements Visitor {
    SymbolTable symbols;

    public ResolveVisitor (SymbolTable syms) {
        this.symbols = syms;
    }

    public void visit (Node node) {
        if (node instanceof ApplyExpression) {
            Symbol resolved = resolveExpression(((ApplyExpression) node).base);
            // Symbol args = 
        } else {
            node.visited(this);
        }
    }

    Symbol resolveExpression (Node node) {
        if (node instanceof Identifier) {
            LongName lname = ((Identifier) node).identifier;
            if (lname.names.length != 1) {
                throw new Error("Identifier should have only one name: " + lname.toString());
            }

            String name = lname.names[0];

            Symbol sym = symbols.get(name);

            // If asked to resolve, an identifier MUST resolve.
            if (sym == null) {
                throw new Error("Cannot find symbol `" + name + "`");
            } else {
                return sym;
            }
        } else if (node instanceof SelectExpression) {
            SelectExpression select = (SelectExpression) node;
            Symbol inner = resolveExpression(select.base);

            Symbol next = inner.getSymbol(select.field);

            if (next == null) {
                throw new Error("Cannot find symbol `" + select.field + "`");
            } else {
                return next;
            }
        } else {
            throw new Error("Usupported resolve for " + node);
        }
    }
}