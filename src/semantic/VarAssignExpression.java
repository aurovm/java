package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.*;

public class VarAssignExpression implements ResolvedExpression {
    public RegisterSymbol sym;
    public ResolvedExpression expr;

    public VarAssignExpression (RegisterSymbol sym, ResolvedExpression expr) {
        this.sym = sym;
        this.expr = expr;
    }

    @Override
    public int compile (MethodDeclarationInfo info, CompiledMethod method) {
        int src = expr.compile(info, method);
        int tgt = sym.getRegister();

        Instruction inst = new Instruction();
        inst.type = Instruction.Type.SET;
        inst.args = new int[]{ tgt, src };
        method.fn.code.add(inst);
        return tgt;
    }

    public ResolvedType getType () {
        return ((ResolvedExpression) this.sym).getType();
    }
}