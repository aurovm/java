package org.auro.java.semantic;

import org.auro.java.auro.Type;

public class AuroTypeSymbol implements Symbol {
    public Type tp;

    public AuroTypeSymbol (Type tp) {
        this.tp = tp;
    }

    @Override
    public Symbol getSymbol (String name) {
        throw new Error("Unimplemented: Type static symbols");
    }
}