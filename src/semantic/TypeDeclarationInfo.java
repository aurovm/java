package org.auro.java.semantic;

import java.util.ArrayList;
import java.util.HashMap;
import org.auro.java.node.*;
import org.auro.java.compile.*;
import org.auro.java.auro.*;

public class TypeDeclarationInfo {
    public CompilationUnitInfo cuInfo;
    public SymbolTable symbols = new SymbolTable();
    public TypeDeclaration node;

    public ArrayList<FieldDeclarationInfo> fields = new ArrayList();
    public ArrayList<MethodDeclarationInfo> methods = new ArrayList();

    public HashMap<String, FieldDeclarationInfo> fieldsByName = new HashMap();
    public HashMap<String, MethodDeclarationInfo> methodsByName = new HashMap();

    public ConstructorInfo constructor;

    public CompiledFile compiled;

    public TypeDeclarationInfo (TypeDeclaration node, CompilationUnitInfo cuInfo) {
        this.cuInfo = cuInfo;
        this.node = node;

        symbols.setParent(cuInfo.symbols);
    }

    public boolean isStatic () {
        return node.modifier._static;
    }

    public boolean isPublic () {
        return node.modifier.visibility == Modifier.Visibility.PUBLIC;
    }

    public void process () {
        if (node instanceof ClassDeclaration) {
            ClassDeclaration decl = (ClassDeclaration) node;

            if (!isStatic()) {
                symbols.set(node.name, new LocalTypeSymbol(this));
            }

            if (decl.types.length > 0) {
                throw new Error("Type declarations in class not supported");
            }

            for (FieldDeclaration field : decl.fields) {
                FieldDeclarationInfo info = new FieldDeclarationInfo(field, this);
                this.fields.add(info);
                fieldsByName.put(field.name, info);
                symbols.set(field.name, new LocalFieldSymbol(info));
            }

            for (MethodDeclaration method : decl.methods) {
                MethodDeclarationInfo info = new MethodDeclarationInfo(method, this);
                this.methods.add(info);
                methodsByName.put(method.name, info);
                symbols.set(method.name, new LocalMethodSymbol(info));
            }

            if (decl.constructors.length > 1) {
                throw new Error("Only a single constructor is supported at the moment");
            }

            if (decl.constructors.length == 1) {
                constructor = new ConstructorInfo(decl.constructors[0], this);
            }

            for (FieldDeclarationInfo field : fields) {
                field.process();
            }

            for (MethodDeclarationInfo method : methods) {
                method.process();
            }

            if (constructor != null) constructor.process();
        }
    }

    void compileConstructor () {
        // TODO: Field Types
        Type[] fieldTypes = new Type[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            fieldTypes[i] = fields.get(i).compileType(compiled);
        }

        ModuleItem recordMod = compiled.createRecordModule(fieldTypes);
        // passing a string automatically exports the type
        compiled.rawType = compiled.importType(recordMod, "");
        compiled.rawConstructor = compiled.importFunction(recordMod, "new", fieldTypes, new Type[]{ compiled.rawType });

        if (isPublic()) {
            compiled.auroModule.items.put("", compiled.rawType);
        }

        CompiledMethod method = new CompiledMethod();
        method.file = compiled;
        method.fn = compiled.auroModule.newFunction("new", new Type[0], new Type[]{ compiled.rawType });
        method.fn.code = new ArrayList();

        compiled.constructor = method;


        for (int i = 0; i < fields.size(); i++) {
            FieldDeclarationInfo field = fields.get(i);
            field.getter = compiled.importFunction(recordMod, "get" + i, new Type[]{compiled.rawType}, new Type[]{field.compiledType});
            field.setter = compiled.importFunction(recordMod, "set" + i, new Type[]{compiled.rawType, field.compiledType}, new Type[0]);
        }

        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = compiled.rawConstructor;
        inst.args = new int[fields.size()];

        for (int i = 0; i < inst.args.length; i++) {
            inst.args[i] = fields.get(i).compileValue(method);
        }

        method.fn.code.add(inst);

        int thisReg = method.regCount;
        method.regCount += method.fn.outs.length;

        Instruction end = new Instruction();
        end.type = Instruction.Type.END;
        end.args = new int[]{ thisReg };
        method.fn.code.add(end);
    }

    public CompiledFile compile () {
        compiled = new CompiledFile();
        compiled.cuInfo = cuInfo;
        compiled.tpInfo = this;

        compiled.setupAuroModule();

        if (!isStatic()) {
            compileConstructor();

            if (constructor != null) {
                constructor.compileStart(compiled);
            }
        }


        for (MethodDeclarationInfo method : methods) {
            method.compileStart(compiled);
        }

        for (MethodDeclarationInfo method : methods) {
            method.compileEnd();
        }

        if (constructor != null) {
            constructor.compileEnd();
        }

        compiled.auroModule.shiftConstants();
        return compiled;
    }
}