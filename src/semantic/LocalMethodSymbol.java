package org.auro.java.semantic;

public class LocalMethodSymbol implements Symbol {
    public MethodDeclarationInfo info;

    public LocalMethodSymbol (MethodDeclarationInfo info) {
        this.info = info;
    }
    
    public Symbol getSymbol (String name) { return null; }
}