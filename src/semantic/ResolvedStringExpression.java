package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.Constant;
import org.auro.java.auro.CallConstant;
import org.auro.java.auro.Instruction;

public class ResolvedStringExpression implements ResolvedExpression {
    public String value;
    public ResolvedStringExpression (String v) { value = v; }

    @Override
    public int compile (MethodDeclarationInfo info, CompiledMethod method) {
        Constant bytes = method.file.auroModule.addConstant(value);

        CallConstant cns = new CallConstant();
        method.file.auroModule.addConstant(cns);
        cns.args = new Constant[]{bytes};
        cns.fn = method.file.newString;

        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = cns;

        method.fn.code.add(inst);

        return method.regCount++;
    }

    public ResolvedType getType () {
        return new PrimitiveTypeSymbol("String");
    }
}