package org.auro.java.semantic;

import java.util.ArrayList;
import org.auro.java.node.*;
import org.auro.java.compile.*;
import org.auro.java.auro.*;

public class FieldDeclarationInfo {
    TypeDeclarationInfo typeInfo;
    FieldDeclaration node;

    SymbolTable symbols = new SymbolTable();

    ResolvedType typeSymbol;
    ResolvedExpression expression;

    public Type compiledType;
    public Function getter;
    public Function setter;

    public FieldDeclarationInfo (FieldDeclaration node, TypeDeclarationInfo typeInfo) {
        this.typeInfo = typeInfo;
        this.node = node;

        symbols.setParent(typeInfo.symbols);
    }

    public void process () {
        typeSymbol = (ResolvedType) node.type.resolveAsSymbol(symbols);

        if (node.value != null) {
            expression = node.value.resolveAsExpression(symbols);
        } else {
            expression = typeSymbol.defaultValue();
        }

        typeInfo.symbols.set(node.name, new LocalFieldSymbol(this));
    }

    public Type compileType (CompiledFile file) {
        compiledType = typeSymbol.compile(file);
        return compiledType;
    }

    public int compileValue (CompiledMethod method) {
        return expression.compile(null, method);
    }
}