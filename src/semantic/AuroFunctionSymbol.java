package org.auro.java.semantic;

import org.auro.java.auro.Function;

public class AuroFunctionSymbol implements Symbol {
    public Function fn;

    public AuroFunctionSymbol (Function fn) {
        this.fn = fn;
    }

    @Override
    public Symbol getSymbol (String name) {
        return null;
    }
}