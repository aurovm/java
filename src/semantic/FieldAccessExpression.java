package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.*;

public class FieldAccessExpression implements ResolvedExpression {
    public FieldDeclarationInfo info;
    public ResolvedExpression instance;

    public FieldAccessExpression (FieldDeclarationInfo info, ResolvedExpression instance) {
        this.info = info;
        this.instance = instance;
    }
    
    public Symbol getSymbol (String name) { return null; }

    @Override
    public int compile (MethodDeclarationInfo info, CompiledMethod method) {
        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = this.info.getter;
        inst.args = new int[]{ instance.compile(info, method) };

        method.fn.code.add(inst);

        return method.regCount++;
    }

    public ResolvedType getType () {
        return this.info.typeSymbol;
    }
}