package org.auro.java.semantic;

import org.auro.java.auro.Type;
import org.auro.java.compile.CompiledFile;

public interface ResolvedType extends Symbol {
    public Type compile (CompiledFile file);
    public ResolvedExpression defaultValue ();
}