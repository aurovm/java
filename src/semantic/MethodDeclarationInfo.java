package org.auro.java.semantic;

import org.auro.java.node.*;
import org.auro.java.compile.*;
import org.auro.java.auro.*;
import java.util.ArrayList;

public class MethodDeclarationInfo {
    public class Parameter {
        public MethodDeclarationInfo method;
        public ResolvedType type;
        public String name;
        public int register;
    }

    TypeDeclarationInfo typeInfo;
    MethodDeclaration node;
    boolean onInstance;

    SymbolTable symbols = new SymbolTable();
    //ArrayList<VariableInfo> variables = new ArrayList();

    ArrayList<ResolvedStatement> body = new ArrayList();
    Parameter[] parameters;

    CompiledMethod compiled;

    public MethodDeclarationInfo (MethodDeclaration node, TypeDeclarationInfo typeInfo) {
        this.typeInfo = typeInfo;
        this.node = node;
        this.onInstance = !node.modifier._static;

        symbols.setParent(typeInfo.symbols);
    }

    public void process () {
        // TODO: process `this`

        int thisConst = 0;
        if (onInstance) {
            thisConst = 1;
            symbols.set("this", new ThisSymbol(this));
        }

        parameters = new Parameter[node.parameters.length];

        for (int i = 0; i < parameters.length; i++) {
            ParameterNode _param = node.parameters[i];
            Parameter param = new Parameter();
            param.method = this;
            param.name = _param.name;
            param.register = i + thisConst;

            // Parameter types are resolved in the scope of the Method declaration,
            // otherwise parameters would see each other
            param.type = (ResolvedType) _param.type.resolveAsSymbol(typeInfo.symbols);
            if (param.type == null) { throw new Error(); }
            parameters[i] = param;

            ParameterSymbol sym = new ParameterSymbol(param);
            symbols.set(param.name, sym);
        }

        for (Node node : node.body.statements) {
            ResolvedStatement[] stmts = node.resolveAsStatements(symbols);
            java.util.Collections.addAll(body, stmts);
        }
    }

    public void compileStart (CompiledFile file) {
        compiled = new CompiledMethod();
        compiled.file = file;

        Type[] ins = new Type[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            ins[i] = parameters[i].type.compile(file);
        }

        if (onInstance) {
            Type[] _ins = ins;
            ins = new Type[ins.length + 1];
            ins[0] = typeInfo.compiled.rawType;
            System.arraycopy(_ins, 0, ins, 1, _ins.length);
        }

        compiled.fn = file.auroModule.newFunction(node.name, ins, new Type[0]);
        compiled.fn.code = new ArrayList();
        compiled.regCount = ins.length;
    }

    public void compileEnd () {
        for (ResolvedStatement stmt : body) {
            stmt.compile(this, compiled);
        }

        Function fn = compiled.fn;
        if (fn.outs.length == 0) {
            if (fn.code.size() == 0 || fn.code.get(fn.code.size() - 1).type != Instruction.Type.END) {
                Instruction end = new Instruction();
                end.type = Instruction.Type.END;
                fn.code.add(end);
            }
        }
    }
}