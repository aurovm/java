package org.auro.java.semantic;

import org.auro.java.auro.ModuleNode;
import org.auro.java.auro.Module;
import org.auro.java.auro.Item;
import org.auro.java.auro.Function;
import org.auro.java.auro.Type;

public class PackageSymbol implements Symbol {
    public boolean partial;

    // If partial == true
    public ModuleNode moduleNode;

    // If partial == false
    public Module module;

    public PackageSymbol (ModuleNode mn) {
        if (mn.module != null) {
            this.partial = false;
            this.module = mn.module;
        } else {
            this.partial = true;
            this.moduleNode = mn;
        }
    }

    public PackageSymbol (Module m) {
        this.partial = false;
        this.module = m;
    }

    @Override
    public Symbol getSymbol (String name) {
        if (partial) {
            ModuleNode mn = moduleNode.get(name);
            if (mn == null) return null;

            return new PackageSymbol(mn);
        } else {
            Item item = this.module.items.get(name);

            if (item instanceof Function) {
                return new AuroFunctionSymbol((Function) item);
            } else if (item instanceof Type) {
                return new AuroTypeSymbol((Type) item);
            } else {
                throw new Error("???");
            }
        }
    }
}