package org.auro.java.semantic;

import org.auro.java.compile.CompiledMethod;
import org.auro.java.auro.*;

public class FieldAssignExpression implements ResolvedExpression {
    public FieldDeclarationInfo info;
    public ResolvedExpression instance;
    public ResolvedExpression value;

    public FieldAssignExpression (FieldDeclarationInfo info, ResolvedExpression instance, ResolvedExpression value) {
        this.info = info;
        this.instance = instance;
        this.value = value;
    }
    
    public Symbol getSymbol (String name) { return null; }

    @Override
    public int compile (MethodDeclarationInfo info, CompiledMethod method) {
        Instruction inst = new Instruction();
        inst.type = Instruction.Type.CALL;
        inst.fn = this.info.setter;
        inst.args = new int[]{ instance.compile(info, method), value.compile(info, method) };

        method.fn.code.add(inst);

        return inst.args[1];
    }

    public ResolvedType getType () {
        return this.info.typeSymbol;
    }
}