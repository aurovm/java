package org.auro.java.compile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.FileOutputStream;
import java.io.IOException;

import org.auro.java.semantic.CompilationUnitInfo;
import org.auro.java.semantic.TypeDeclarationInfo;
import org.auro.java.semantic.VarSymbol;

import org.auro.java.auro.*;
import org.auro.java.auro.Module;

// When compiled, each file represents a single class
public class CompiledFile {
    class ImportKey {
        ModuleItem module;
        String name;
        public ImportKey (ModuleItem m, String n) {
            if (m == null || n == null) throw new NullPointerException();
            module = m; name = n;
        }
        @Override
        public boolean equals (Object o) {
            if (!(o instanceof ImportKey)) return false;
            ImportKey x = (ImportKey) o;
            return x.name == name && x.module == module;
        }
        @Override
        public int hashCode () { return name.hashCode() ^ module.hashCode(); }
    }

    public CompilationUnitInfo cuInfo;
    public TypeDeclarationInfo tpInfo;

    public ArrayList<CompiledImportedClass> classes = new ArrayList();

    public ArrayList<CompiledMethod> methods = new ArrayList();

    public CompiledTypeResolver typeResolver = new CompiledTypeResolver(this);

    HashMap<String, ImportedModule> importedModules = new HashMap();
    HashMap<ImportKey, Function> importedFunctions = new HashMap();
    HashMap<ImportKey, Type> importedTypes = new HashMap();

    HashMap<Type, BuildModule> nullModules = new HashMap();

    // Can be null
    public Type rawType;
    public Function rawConstructor;
    public CompiledMethod constructor;

    public Module auroModule = new Module();

    public Function newString;

    public ImportedModule importModule (String name) {
        ImportedModule mod = importedModules.get(name);
        if (mod == null) {
            mod = new ImportedModule();
            mod.name = name;
            auroModule.addModule(mod);
            importedModules.put(name, mod);
        }
        return mod;
    }

    public Type importType (ModuleItem module, String name) {
        ImportKey key = new ImportKey(module, name);
        Type tp = importedTypes.get(key);
        if (tp == null) {
            tp = auroModule.newType(null);
            tp.base = module;
            tp.name = name;
            importedTypes.put(key, tp);
        }
        return tp;
    }

    public Type importType (String module, String name) {
        return importType(importModule(module), name);
    }

    public Type importType (Type src) {
        if (src.module == auroModule) { return src; }
        return importType(src.module.path, src.name);
    }

    public Function importFunction (ModuleItem module, String name, Type[] ins, Type[] outs) {
        ins = java.util.Arrays.copyOf(ins, ins.length);
        outs = java.util.Arrays.copyOf(outs, outs.length);

        for (int i = 0; i < ins.length; i++) { ins[i] = importType(ins[i]); }
        for (int i = 0; i < outs.length; i++) { outs[i] = importType(outs[i]); }

        ImportKey key = new ImportKey(module, name);
        Function fn = importedFunctions.get(key);
        if (fn == null) {
            fn = auroModule.newFunction(null, ins, outs);
            fn.base = module;
            fn.name = name;
            importedFunctions.put(key, fn);
        }
        return fn;
    }

    public Function importFunction (String module, String name, Type[] ins, Type[] outs) {
        return importFunction(importModule(module), name, ins, outs);
    }

    public Function importFunction (Function src) {
        if (src.module == auroModule) { return src; }
        return importFunction(src.module.path, src.name, src.ins, src.outs);
    }

    public BuildModule createRecordModule (Type[] types) {
        ImportedModule record = importModule("auro\u001frecord");
        DefineModule argument = new DefineModule();
        auroModule.addModule(argument);
        for (int i = 0; i < types.length; i++) {
            argument.items.put("" + i, importType(types[i]));
        }

        BuildModule build = new BuildModule();
        build.base = record;
        build.argument = argument;
        auroModule.addModule(build);

        return build;
    }

    public ModuleItem nullableModule (Type type) {
        type = importType(type);
        ModuleItem module = nullModules.get(type);
        if (module != null) { return module; }

        ImportedModule base = importModule("auro\u001frecord");
        DefineModule argument = new DefineModule();
        auroModule.addModule(argument);
        argument.items.put("0", type);

        BuildModule build = new BuildModule();
        build.base = base;
        build.argument = argument;
        auroModule.addModule(build);

        return build;
    }

    public void setupAuroModule () {
        DefineModule def = new DefineModule();
        def.items = auroModule.items;

        auroModule.addModule(def);

        Type buf = importType("auro\u001fbuffer", "buffer");
        Type str = importType("auro\u001fstring", "string");
        newString = importFunction("auro\u001fstring", "new", new Type[]{buf}, new Type[]{str});
    }

    public void writeToFile (String path) {
        try {
            auroModule.writeToFile(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}