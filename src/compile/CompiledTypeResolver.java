package org.auro.java.compile;

import java.util.HashMap;
import org.auro.java.auro.ImportedModule;
import org.auro.java.node.*;

public class CompiledTypeResolver {
    public CompiledFile file;

    public HashMap<String, CompiledType> prims = new HashMap();

    public ImportedModule strmod;
    public ImportedModule intmod;

    public CompiledTypeResolver (CompiledFile file) {
        this.file = file;
    }

    public CompiledType resolvePrimitive (String name) {
        if (name == "String") {

        }
        throw new Error("Unimplemented: Resolve primitive type. " + name);
    }

    public CompiledType resolve (TypeNode node) {
        if (node instanceof BaseTypeNode) {
            String[] names = ((BaseTypeNode) node).identifier.names;
            if (names.length == 1) {
                String name = names[0];

                if (name.equals("int")) {
                    return resolvePrimitive("int");
                } else if (name.equals("String")) {
                    return resolvePrimitive("String");
                }
            }

        }
        throw new Error("Unimplemented: Resolve compiled type. " + node);
    } 
}