package org.auro.java.compile;

import java.util.HashMap;

import org.auro.java.auro.Function;
import org.auro.java.semantic.Symbol;

// When compiled, each file represents a single class
public class CompiledMethod {
    public String name;
    public boolean isStatic;

    public CompiledFile file;
    public Function fn;

    public HashMap<Symbol, Integer> variables = new HashMap();
    public int regCount = 0;
}