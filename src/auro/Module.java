package org.auro.java.auro;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;

public class Module {
    // Optional fields describing where the module comes from and it's purpose
    public File baseFile;
    public String path;
    public boolean builtin = false;

    public HashMap<String, Item> items = new HashMap();

    public ArrayList<Function> functions = new ArrayList();
    public ArrayList<Constant> constants = new ArrayList();
    public ArrayList<Type> types = new ArrayList();
    public ArrayList<ModuleItem> modules = new ArrayList();

    boolean constantShifted = false;

    public Type newType (String name) {
        Type tp = new Type();
        tp.name = name;
        tp.module = this;
        tp.index = types.size();
        types.add(tp);
        if (name != null) {
            items.put(name, tp);
        }
        return tp;
    }

    public Function newFunction (String name, Type[] ins, Type[] outs) {
        if (constantShifted) { throw new Error("Constants already shifted"); }
        Function fn = new Function();
        fn.name = name;
        fn.ins = ins;
        fn.outs = outs;
        fn.module = this;
        fn.index = functions.size();
        functions.add(fn);
        if (name != null) {
            items.put(name, fn);
        }
        return fn;
    }

    public Constant addConstant (Object val) {
        Constant cns;
        if (val instanceof Constant) {
            cns = (Constant) val;
        } else {
            cns = new Constant();
            cns.value = val;
        }

        cns.index = constants.size();
        constants.add(cns);
        return cns;
    }

    public void addModule (ModuleItem mod) {
        // Index 0 is reserved for the argument module
        mod.index = modules.size() + 1;
        modules.add(mod);
    }

    public void shiftConstants () {
        if (constantShifted) { throw new Error("Constants already shifted"); }
        for (Constant cns : constants) {
            cns.index += functions.size();
        }
        constantShifted = true;
    }

    byte[] intBytes (int n) {
        if (n > 127) {
            throw new Error("Cannot write big integers");
        }
        return new byte[]{ (byte) n };
    }

    byte[] strBytes (String s) {
        try {
            byte[] data = s.getBytes("UTF-8");
            byte[] len = intBytes(data.length);

            byte[] b = java.util.Arrays.copyOf(len, len.length + data.length);
            System.arraycopy(data, 0, b, len.length, data.length);
            return b;
        } catch (Exception e) {
            throw new Error(e.toString());
        }
    }

    void writeModules(FileOutputStream fos) throws IOException {
        fos.write(intBytes(modules.size()));

        for (ModuleItem mod : modules) {
            if (mod instanceof ImportedModule) {
                fos.write(1);
                fos.write(strBytes(((ImportedModule) mod).name));
            } else if (mod instanceof DefineModule) {
                fos.write(2);

                HashMap<String, Item> items = ((DefineModule) mod).items;
                fos.write(intBytes(items.size()));

                for (String key : items.keySet()) {
                    Item item = items.get(key);

                    int kind;
                    if (item instanceof Type) {
                        kind = 1;
                    } else if (item instanceof Function) {
                        kind = 2;
                    } else if (item instanceof ModuleItem) {
                        kind = 0;
                    } else {
                        throw new Error("What is this item? " + item);
                    }

                    fos.write(kind);
                    fos.write(item.index);
                    fos.write(strBytes(key));
                }
            } else if (mod instanceof BuildModule) {
                BuildModule build = (BuildModule) mod;
                fos.write(4);
                fos.write(intBytes(build.base.index));
                fos.write(intBytes(build.argument.index));
            } else {
                throw new Error("Unsupported module: " + mod);
            }
        }

    }

    void writeTypes(FileOutputStream fos) throws IOException {
        fos.write(intBytes(types.size()));
        for (Type t : types) {
            fos.write(intBytes(t.base.index + 1));
            fos.write(strBytes(t.name));
        }
    }

    void writeFunctions(FileOutputStream fos) throws IOException {
        fos.write(intBytes(functions.size()));
        for (Function f : functions) {
            // NULL (Unknown, empty) function kind
            if (f.code != null) {
                fos.write(1);
            } else if (f.base != null) {
                fos.write(f.base.index + 2);
            } else {
                fos.write(0);
            }

            fos.write(intBytes(f.ins.length));
            for (Type t : f.ins) {
                fos.write(intBytes(t.index));
            }

            fos.write(intBytes(f.outs.length));
            for (Type t : f.outs) {
                fos.write(intBytes(t.index));
            }

            if (f.base != null) {
                fos.write(strBytes(f.name));
            }
        }
    }

    void writeConstants(FileOutputStream fos) throws IOException {
        fos.write(intBytes(constants.size()));
        for (Constant cns : constants) {
            if (cns instanceof CallConstant) {
                CallConstant call = (CallConstant) cns;
                fos.write(intBytes(call.fn.index + 16));
                for (Function arg : call.args) {
                    fos.write(intBytes(arg.index));
                }
            } else if (cns.value instanceof Integer) {
                int value = ((Integer) cns.value).intValue();
                fos.write(1);
                fos.write(intBytes(value));
            } else if (cns.value instanceof String) {
                fos.write(2);
                fos.write(strBytes((String) cns.value));
            } else {
                throw new Error("Unsupported constant: " + cns.value);
            }
        }
    }

    void writeCode(FileOutputStream fos) throws IOException {
        for (Function f : functions) {
            if (f.code != null) {
                fos.write(intBytes(f.code.size()));

                for (Instruction inst : f.code) {
                    switch (inst.type) {
                        case END: {
                            fos.write(0);
                            for (int arg : inst.args) {
                                fos.write(intBytes(arg));
                            }
                            break;   
                        }
                        case SET: {
                            fos.write(4);
                            fos.write(intBytes(inst.args[0]));
                            fos.write(intBytes(inst.args[1]));
                            break;
                        }
                        case CALL: {
                            fos.write(intBytes(inst.fn.index + 16));

                            for (int arg : inst.args) {
                                fos.write(intBytes(arg));
                            }
                            break;
                        }
                        default: {
                            throw new Error("Unsupported instruction " + inst.type);
                        }
                    }
                }
            }
        }
    }

    void writeMetadata(FileOutputStream fos) throws IOException {
        fos.write(0);
    }

    public void writeToFile (String path) throws IOException {
        if (!constantShifted) { throw new Error("Constants have not been shifted"); }
        FileOutputStream fos = new FileOutputStream(new File(path));

        fos.write("Auro 0.6\u0000".getBytes("UTF-8"));

        writeModules(fos);
        writeTypes(fos);
        writeFunctions(fos);
        writeConstants(fos);
        writeCode(fos);
        writeMetadata(fos);

        fos.close();
    }
}