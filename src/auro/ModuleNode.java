package org.auro.java.auro;

import java.util.HashMap;

public class ModuleNode {
    public HashMap<String, ModuleNode> subnodes = new HashMap();
    public Module module = null;

    public ModuleNode get (String name) {
        return subnodes.get(name);
    }

    ModuleNode forceGet (String name) {
        ModuleNode node = subnodes.get(name);

        if (node == null) {
            node = new ModuleNode();
            subnodes.put(name, node);
        }

        return node;
    }

    Module createModule (String path) {
        if (module != null) {
            throw new Error("Node already has module. When trying to create " + path);
        }
        module = new Module();
        module.path = path;
        return module;
    }
}