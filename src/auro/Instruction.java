package org.auro.java.auro;

public class Instruction {
    public enum Type {
        END, HLT, VAR, DUP, SET, JMP, JIF, NIF, CALL,
    }

    public Type type;
    public int[] args = new int[0];

    // Function for CALL
    public Function fn;

    // Instruction index for JIF and NIF
    public int inst;
}