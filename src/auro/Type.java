package org.auro.java.auro;

public class Type extends Item {
    // For types that are imported from a module
    public ModuleItem base;
    public String name;
}