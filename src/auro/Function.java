package org.auro.java.auro;

import java.util.ArrayList;

public class Function extends Item {
    public Type[] ins;
    public Type[] outs;

    // For functions with code
    public ArrayList<Instruction> code;

    // For imported functions
    public ModuleItem base;
    public String name;
}