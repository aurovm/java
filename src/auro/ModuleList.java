package org.auro.java.auro;

import java.io.File;

public class ModuleList {
    public ModuleNode root = new ModuleNode();

    public ModuleNode forceGet (String[] path) {
        ModuleNode node = root;

        for (String segment : path) {
            node = node.forceGet(segment);
        }

        return node;
    }

    public void addSearchPath (String dirpath) {
        try {
            File dirfile = new File(dirpath);
            File[] files = dirfile.listFiles();

            for (File file : files) {
                String name = file.getName();

                // This is a regex, not just a string
                String[] segments = name.split("\\.");

                // TODO: Add to the module list
                //System.out.println("Module " + name + ", " + segments.length + " segments");

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void addStandard () {
        Module strmod = forceGet(new String[]{"auro", "string"}).createModule("auro\u001fstring");
        Type stringType = strmod.newType("string");

        Module sysmod = forceGet(new String[]{"auro", "system"}).createModule("auro\u001fsystem");
        sysmod.newFunction("println", new Type[]{ stringType }, new Type[]{});
    }

    static public ModuleList fromSearchPaths (Iterable<String> paths) {
        ModuleList ml = new ModuleList();

        for (String path : paths) {
            ml.addSearchPath(path);
        }

        return ml;
    }
}