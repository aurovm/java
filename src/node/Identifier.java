package org.auro.java.node;

import org.auro.java.semantic.*;

public class Identifier extends Expression {
    public LongName identifier;

    public Identifier (LongName identifier) {
        this.identifier = identifier;
    }

    @Override
    public Symbol resolveAsSymbol (SymbolTable symbols) {
        Symbol sym = symbols.get(identifier.names[0]);

        if (sym == null) {
            throw new Error("Cannot find symbol `" + identifier.names[0] + "`");
        }

        for (int i = 1; i < identifier.names.length; i++) {
            String name = identifier.names[i];
            sym = sym.getSymbol(name);
            if (sym == null) {
                throw new Error("Cannot find symbol `" + name + "`");
            }
        }

        return sym;
    }

    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        return (ResolvedExpression) resolveAsSymbol(symbols);
    }

    @Override
    public TypeNode toType () {
        return new BaseTypeNode(identifier);
    }
}