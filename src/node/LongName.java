package org.auro.java.node;

public class LongName {
    public final String[] names;
    public LongName (String[] ns) {
        this.names = ns;
    }

    public String toString () {
        return String.join(".", names);
    }
}