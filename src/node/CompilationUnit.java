package org.auro.java.node;

public class CompilationUnit extends Node {
    public LongName packageDecl;
    //public Import[] imports;
    public TypeDeclaration[] types;

    public String toString () {
        String r = "CompilationUnit(";
        if (packageDecl != null) {
            r += "package " + packageDecl;
        }

        for (TypeDeclaration t : types) {
            r += "," + t.toString();
        }

        return r + ")";
    }

    @Override
    public void print (String indent) {
        System.out.println(indent + "package " + packageDecl + " {");
        for (TypeDeclaration t : types) {
            t.printIndent(indent);
        }
        System.out.println(indent + "}");
    }

    @Override
    public void visited (Visitor visitor) {
        for (TypeDeclaration t : types) {
            visitor.visit(t);
        }
    }
}