package org.auro.java.node;

//import org.auro.java.semantic.MethodRef;

import org.auro.java.semantic.Symbol;
import org.auro.java.semantic.SymbolTable;
import org.auro.java.semantic.ResolvedExpression;
import org.auro.java.semantic.ResolvedCallExpression;

public class ApplyExpression extends Expression {
    // base is the base syntax node of the apply when unresolved, but
    // if there's a resolvedMethod, base is the expression for the instance
    // where the method belongs, or null if it's a static call
    public Expression base;
    public Expression[] arguments;

    //public MethodRef resolvedMethod;

    public ApplyExpression (Expression base, Expression[] arguments) {
        this.base = base;
        this.arguments = arguments;
    }

    @Override
    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        Symbol method;
        ResolvedExpression instance = null;
        if (base instanceof SelectExpression) {
            SelectExpression select = (SelectExpression) base;
            method = select.tryResolveAsSymbol(symbols);
            if (method == null) {
                instance = select.base.resolveAsExpression(symbols);
                method = instance.getType().getSymbol(select.field);
            }
        } else {
            method = base.resolveAsSymbol(symbols);
        }

        ResolvedExpression[] args = new ResolvedExpression[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            args[i] = arguments[i].resolveAsExpression(symbols);
        }
        return new ResolvedCallExpression(method, instance, args);
    }

    @Override
    public void visited (Visitor visitor) {
        visitor.visit(base);
        for (Expression arg : arguments) {
            visitor.visit(arg);
        }
    }
}