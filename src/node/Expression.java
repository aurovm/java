package org.auro.java.node;

public abstract class Expression extends Node {
    // Expressions are not assignable by default
    public boolean isAsignable () { return false; }

    public TypeNode toType () { return null; }
}