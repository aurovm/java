package org.auro.java.node;

//import org.auro.java.semantic.MethodRef;

import org.auro.java.semantic.*;

public class NewExpression extends Expression {
    public TypeNode type;
    public Expression[] arguments;

    public NewExpression (TypeNode type, Expression[] arguments) {
        this.type = type;
        this.arguments = arguments;
    }

    @Override
    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        Symbol type = this.type.resolveAsSymbol(symbols);

        Symbol method;
        if (type instanceof LocalTypeSymbol) {
            TypeDeclarationInfo info = ((LocalTypeSymbol) type).info;
            if (info.node.modifier._static) {
                throw new Error("Type " + info.node.name + " cannot be instantiated");
            }
            method = new ConstructorSymbol(info);
        } else {
            throw new Error("Unimplemented");
        }

        ResolvedExpression[] args = new ResolvedExpression[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            args[i] = arguments[i].resolveAsExpression(symbols);
        }
        return new ResolvedCallExpression(method, null, args);
    }
}