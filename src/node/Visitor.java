package org.auro.java.node;

public interface Visitor {
    public void visit (Node node);
}