package org.auro.java.node;

import org.auro.java.semantic.SymbolTable;
import org.auro.java.semantic.ResolvedExpression;
import org.auro.java.semantic.ResolvedLiteralExpression;

public class IntLiteral extends Expression {
    public final int value;
    public IntLiteral (int value) {
        this.value = value;
    }

    public String toString () {
        return "" + value;
    }

    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        return new ResolvedLiteralExpression(value);
    }
}