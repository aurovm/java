package org.auro.java.node;

import org.auro.java.semantic.SymbolTable;
import org.auro.java.semantic.ResolvedStatement;
import org.auro.java.semantic.ResolvedExpression;
import org.auro.java.semantic.ResolvedExpressionStatement;

public class ExpressionStatement extends StatementNode {
    public Expression expr;

    public ExpressionStatement (Expression expr) {
        this.expr = expr;
    }

    @Override
    public ResolvedStatement resolveAsStatement (SymbolTable symbols) {
        ResolvedExpression expr = this.expr.resolveAsExpression(symbols);
        return new ResolvedExpressionStatement(expr);
    }

    @Override
    public String toString () {
        return expr.toString() + ";";
    }

    @Override
    public void visited (Visitor visitor) {
        visitor.visit(expr);
    }
}