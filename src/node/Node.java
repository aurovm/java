package org.auro.java.node;

import org.auro.java.semantic.ResolvedStatement;
import org.auro.java.semantic.ResolvedExpression;
import org.auro.java.semantic.SymbolTable;
import org.auro.java.semantic.Symbol;

public abstract class Node {
    public void print (String indentation) {
        System.out.println(indentation + this.toString());
    }

    public void printIndent (String indentation) {
        print(indentation + "  ");
    }

    public void visited (Visitor visitor) {}

    public Symbol resolveAsSymbol (SymbolTable symbols) {
        throw new Error("Node is not resolvable as a symbol: " + this);
    }

    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        throw new Error("Node is not resolvable as an expression: " + this);
    }

    public ResolvedStatement resolveAsStatement (SymbolTable symbols) {
        throw new Error("Node is not resolvable as a statement: " + this);
    }

    public ResolvedStatement[] resolveAsStatements (SymbolTable symbols) {
        return new ResolvedStatement[]{ resolveAsStatement(symbols) };
    }
}