package org.auro.java.node;

public class ArrayTypeNode extends TypeNode {
    public TypeNode base;
    public ArrayTypeNode (TypeNode b) {
        this.base = b;
    }
}