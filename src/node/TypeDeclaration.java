package org.auro.java.node;

public abstract class TypeDeclaration extends Node {
    public String name;
    public Modifier modifier;
}