package org.auro.java.node;

import org.auro.java.semantic.*;

public class SelectExpression extends Expression {
    public Expression base;
    public String field;

    public SelectExpression (Expression base, String field) {
        this.base = base;
        this.field = field;
    }

    public Symbol tryResolveAsSymbol (SymbolTable symbols) {
        if (base instanceof SelectExpression) {
            Symbol sym = ((SelectExpression) base).tryResolveAsSymbol(symbols);
            if (sym != null) {
                return sym.getSymbol(field);
            } else {
                return null;
            }
        } else if (base instanceof Identifier) {
            Symbol sym = base.resolveAsSymbol(symbols);
            return sym.getSymbol(field);
        } else {
            return null;
        }
    }

    @Override
    public Symbol resolveAsSymbol (SymbolTable symbols) {
        Symbol sym = tryResolveAsSymbol(symbols);
        if (sym == null) {
            throw new Error("Cannot resolve " + this);
        }
        return sym;
    }

    @Override
    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        ResolvedExpression base = this.base.resolveAsExpression(symbols);
        ResolvedType tp = base.getType();

        Symbol fieldSym = tp.getSymbol(field);
        if (fieldSym == null) {
            throw new Error("Symbol not found: " + this.field);
        }

        if (fieldSym instanceof LocalFieldSymbol) {
            return new FieldAccessExpression(((LocalFieldSymbol) fieldSym).info, base);
        } else {
            throw new Error(this.field + " is not a field");
        }
    }

    @Override
    public void visited (Visitor visitor) {
        visitor.visit(base);
    }
}