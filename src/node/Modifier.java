package org.auro.java.node;

public class Modifier {
    public enum Visibility {
        DEFAULT, PUBLIC, PRIVATE, PROTECTED,
    };

    public Visibility visibility = Visibility.DEFAULT;
    public boolean _static;
    public boolean _abstract;
    public boolean _final;

    public boolean isEmpty () {
        return visibility == Visibility.DEFAULT && !_static && _abstract && _final;
    }

    /*public String toString () {
        String s = "Modifier(";
        return String.join(".", names);
    }*/
}