package org.auro.java.node;

public class ParameterNode {
    public TypeNode type;
    public String name;

    public ParameterNode (TypeNode tp, String nm) {
        this.type = tp;
        this.name = nm;
    }

    @Override
    public String toString () {
        return type.toString() + " " + name;
    }
}