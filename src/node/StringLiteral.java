package org.auro.java.node;

import org.auro.java.semantic.SymbolTable;
import org.auro.java.semantic.ResolvedExpression;
import org.auro.java.semantic.ResolvedStringExpression;

public class StringLiteral extends Expression {
    public final String value;
    public StringLiteral (String value) {
        this.value = value;
    }

    public String toString () {
        return '"' + value + '"';
    }

    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        return new ResolvedStringExpression(value);
    }
}