package org.auro.java.node;

public class FieldDeclaration extends Node {
    public Modifier modifier;
    public TypeNode type;
    public String name;
    public Expression value;
}