package org.auro.java.node;

import org.auro.java.semantic.*;

public class VarDeclaration extends StatementNode {
    public TypeNode tp;
    public String name;

    public Expression expr;

    public VarDeclaration (TypeNode tp, String name, Expression expr) {
        this.tp = tp;
        this.name = name;
        this.expr = expr;
    }

    @Override
    public String toString () {
        return tp + " " + name + (expr != null ? " = " + expr : "") + ";";
    }

    @Override
    public ResolvedStatement[] resolveAsStatements (SymbolTable symbols) {
        VarSymbol sym = new VarSymbol((ResolvedType) this.tp.resolveAsSymbol(symbols), name);
        symbols.set(name, sym);
        if (expr == null) {
            return new ResolvedStatement[0];
        } else {
            return new ResolvedStatement[]{ new VarDeclStatement(sym, expr.resolveAsExpression(symbols)) };
        }
    }
}