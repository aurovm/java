package org.auro.java.node;

public class MethodDeclaration extends Node {
    public Modifier modifier;
    public TypeNode type;
    public String name;
    public ParameterNode[] parameters;
    //public TypeNode[] _throws;
    public BlockStatement body;

    @Override
    public void print (String indent) {
        String params = "";
        for (ParameterNode p : parameters) {
            params += p.toString() + ", ";
        }
        System.out.println(indent + (type != null ? type.toString() : "void") + " " + name + "(" + params + ")");
        body.print(indent);
    }

    @Override
    public void visited (Visitor visitor) {
        // TODO: Visit parameter types

        visitor.visit(body);
    }
}