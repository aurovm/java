package org.auro.java.node;

public class ClassDeclaration extends TypeDeclaration {
    public LongName superclass;

    public TypeDeclaration[] types;
    public FieldDeclaration[] fields;
    public MethodDeclaration[] methods;
    public MethodDeclaration[] constructors;

    @Override
    public void print (String indent) {
        System.out.println(indent + "class " + name + (superclass != null ? " extends " + this.superclass.toString() : "") + " {");

        for (TypeDeclaration t : types) {
            t.printIndent(indent);
        }

        for (FieldDeclaration f : fields) {
            f.printIndent(indent);
        }

        for (MethodDeclaration m : methods) {
            m.printIndent(indent);
        }

        System.out.println(indent + "}");
    }

    @Override
    public void visited (Visitor visitor) {
        for (TypeDeclaration t : types) {
            visitor.visit(t);
        }

        for (FieldDeclaration f : fields) {
            visitor.visit(f);
        }

        for (MethodDeclaration m : methods) {
            visitor.visit(m);
        }
    }
}