package org.auro.java.node;

public class BlockStatement extends StatementNode {
    public StatementNode[] statements;

    @Override
    public void print (String indent) {
        System.out.println(indent + "{");
        for (StatementNode st : statements) {
            st.printIndent(indent);
        }
        System.out.println(indent + "}");
    }

    @Override
    public void visited (Visitor visitor) {
        for (StatementNode st : statements) {
            visitor.visit(st);
        }
    }
}