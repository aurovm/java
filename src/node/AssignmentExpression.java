package org.auro.java.node;

import org.auro.java.semantic.*;

public class AssignmentExpression extends Expression {
    public Expression left;
    public Expression right;

    public AssignmentExpression (Expression l, Expression r) {
        this.left = l;
        this.right = r;
    }

    @Override
    public ResolvedExpression resolveAsExpression (SymbolTable symbols) {
        if (left instanceof Identifier) {
            Symbol leftSym = left.resolveAsSymbol(symbols);
            if (leftSym instanceof RegisterSymbol) {
                return new VarAssignExpression((RegisterSymbol) leftSym, right.resolveAsExpression(symbols));
            }
        } else if (left instanceof SelectExpression) {
            SelectExpression select = (SelectExpression) left;
            ResolvedExpression base = select.base.resolveAsExpression(symbols);
            ResolvedType tp = base.getType();

            Symbol fieldSym = tp.getSymbol(select.field);
            if (fieldSym == null) {
                throw new Error("Symbol not found: " + select.field);
            }

            if (fieldSym instanceof LocalFieldSymbol) {
                return new FieldAssignExpression(((LocalFieldSymbol) fieldSym).info, base, right.resolveAsExpression(symbols));
            } else {
                throw new Error(select.field + " is not a field");
            }
        }

        throw new Error(left + " is not assignable");

    }
}