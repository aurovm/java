package org.auro.java;

import java.io.*;

import org.auro.java.parser.*;
import org.auro.java.node.*;
import org.auro.java.semantic.*;
import org.auro.java.compile.*;
import org.auro.java.auro.ModuleList;

public class Main {
    static class MyVisitor implements Visitor {
        public void visit (Node node) {
            if (node instanceof ApplyExpression) {
                System.out.println(node.toString());
            } else {
                node.visited(this);
            }
        }
    }

    public static void main (String[] args) throws java.io.IOException, ParseException {
        if (args.length < 1) {
            System.err.println("No file was specified");
            return;
        }

        Reader r;
        try {
            r = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
        } catch (FileNotFoundException e) {
            System.err.println("File " + args[0] + " can't be opened");
            return;
        }

        Lexer lexer = new Lexer(r);

        PackageParser p = new PackageParser(lexer);
        CompilationUnit content = p.parse();

        ModuleList moduleList = new ModuleList();
        moduleList.addStandard();
        moduleList.addSearchPath("/home/arnaud/.auro/modules/");

        GlobalInfo globalInfo = new GlobalInfo(moduleList);
        CompilationUnitInfo cuInfo = new CompilationUnitInfo(content, globalInfo);

        globalInfo.createModuleSymbols();
        globalInfo.createPrimitiveSymbols();

        cuInfo.process();
        CompiledFile[] compiled = cuInfo.compile();

        compiled[0].writeToFile("out/test");

        //content.print("");

        //new MyVisitor().visit(content);
    }
}