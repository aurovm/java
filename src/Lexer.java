package org.auro.java;

import java.io.StreamTokenizer;
import java.io.Reader;
import java.util.ArrayList;

import org.auro.java.Token.Type;

public class Lexer {

    private StreamTokenizer tokenizer;

    private Token next;
    private ArrayList<Token> pool = new ArrayList();

    public Lexer (Reader r) throws java.io.IOException {
        // Take advantage of Java's builting tokenizer
        this.tokenizer = new StreamTokenizer(r);

        // Configure Java's tokenizer
        this.tokenizer.slashSlashComments(true);
        this.tokenizer.slashStarComments(true);
        this.tokenizer.eolIsSignificant(false);
        this.tokenizer.quoteChar('"');
        this.tokenizer.ordinaryChar('.');

        this.next = readNextToken();
    }

    // Replenishes the next character by advancing the input stream
    private Token readNextToken () throws java.io.IOException {
        tokenizer.nextToken();

        int tt = tokenizer.ttype;
        switch (tt) {

            case StreamTokenizer.TT_EOF:
                return null;

            case StreamTokenizer.TT_NUMBER:
                return new Token(Type.NUMBER, "" + tokenizer.nval);

            case StreamTokenizer.TT_WORD:
                return (new Token(Type.IDENTIFIER, tokenizer.sval)).maybePromoteKeyword();

            case StreamTokenizer.TT_EOL:
                return readNextToken();

            case '"':
                return new Token(Type.STRING, tokenizer.sval);

            default:
                return new Token(Type.SYMBOL, "" + (char) tokenizer.ttype);
        }
    }

    public Token next () throws java.io.IOException {
        Token next = this.next;
        if (pool.size() > 0) {
            this.next = pool.remove(0);
        } else {
            this.next = readNextToken();
        }
        return next;
    }

    public void skip () throws java.io.IOException {
        next();
    }

    public Token peek () {
        return this.next;
    }

    public Token peek (int n) throws java.io.IOException {
        if (n < 0) {
            throw new Error("Invalid index for peeking: " + n);
        }

        if (n == 0) {
            return this.next;
        } else {
            // Get the index in the pool
            int i = n - 1;

            // Fill the pool until necessary
            while (pool.size() <= i) {
                Token nxt = this.readNextToken();

                // EOF. Abort.
                if (nxt == null) {
                    return null;
                } else {
                    pool.add(nxt);
                }
            }

            return pool.get(i);
        }
    }
}