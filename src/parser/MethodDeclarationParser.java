package org.auro.java.parser;

import java.util.ArrayList;
import java.io.IOException;
import org.auro.java.Token;
import org.auro.java.Lexer;
import org.auro.java.node.*;

public class MethodDeclarationParser extends Parser {
    MethodDeclaration method;
    StatementParser statementParser;

    public MethodDeclarationParser (Lexer lx) {
        super(lx);
        statementParser = new StatementParser(lx);
    }

    public MethodDeclaration parseAfterVoid () throws IOException, ParseException {
        String name = expectIdent();
        expectSymbol("(");

        return parseAfterParen(null, name);
    }

    public MethodDeclaration parseAfterParen (TypeNode tp, String nm) throws IOException, ParseException {
        method = new MethodDeclaration();
        method.type = tp;
        method.name = nm;

        ArrayList<ParameterNode> params = new ArrayList();

        if (!tryConsumeSymbol(")")) {
            while (true) {
                TypeNode type = parseType();
                String name = expectIdent();

                params.add(new ParameterNode(type, name));

                if (tryConsumeSymbol(")")) {
                    break;
                } else if (!tryConsumeSymbol(",")) {
                    throwExpected("',' or ')'");
                }
            }
        }

        method.parameters = params.toArray(new ParameterNode[]{});

        if (tryConsumeSymbol(";")) {
            method.body = null;
        } else {
            method.body = statementParser.parseBlock();
        }

        return method;
    }
}