package org.auro.java.parser;

import java.util.ArrayList;
import org.auro.java.Token;
import org.auro.java.Lexer;
import org.auro.java.node.*;

public class PackageParser extends Parser {
    public CompilationUnit content = new CompilationUnit();

    public PackageParser (Lexer lx) {
        super(lx);
    }

    void parsePackageDeclaration () throws java.io.IOException, ParseException {
        if (tryConsumeKeyword("package")) {
            LongName name = parseLongName();
            if (name == null) {
                throwExpected("identifier");
            }

            expectSymbol(";");

            content.packageDecl = name;
        }
    }

    void parseImportDeclarations () throws java.io.IOException, ParseException {
        while (tryConsumeKeyword("import")) {
            throw new Error("import not yet supported");
        }
    }

    void parseTypeDeclarations () throws java.io.IOException, ParseException {
        ArrayList<TypeDeclaration> types = new ArrayList();

        TypeDeclarationParser typeParser = new TypeDeclarationParser(lexer);

        // Parse until the end of the input!
        while (lexer.peek() != null) {
            Modifier modifier = parseModifier();

            TypeDeclaration type = typeParser.parse();
            if (type == null) {
                throwExpected("type declaration");
            }
            type.modifier = modifier;
            types.add(type);
        }

        content.types = types.toArray(new TypeDeclaration[]{});
    }

    public CompilationUnit parse () throws java.io.IOException, ParseException {
        parsePackageDeclaration();
        parseImportDeclarations();
        parseTypeDeclarations();
        return this.content;
    }
}