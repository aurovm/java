package org.auro.java.parser;

import java.util.ArrayList;
import java.io.IOException;
import org.auro.java.*;
import org.auro.java.node.*;

public abstract class Parser {
    public Lexer lexer;

    public Parser (Lexer lx) {
        this.lexer = lx;
    }

    boolean tryConsumeKeyword (String kw) throws IOException {
        if (lexer.peek().isKeyword(kw)) {
            lexer.next();
            return true;
        } else {
            return false;
        }
    }

    boolean tryConsumeSymbol (String sym) throws IOException {
        if (lexer.peek().isSymbol(sym)) {
            lexer.next();
            return true;
        } else {
            return false;
        }
    }

    void expectSymbol (String sym) throws IOException, ParseException {
        if (!tryConsumeSymbol(sym)) {
            throwExpected(sym);
        }
    }

    String expectIdent () throws IOException, ParseException {
        if (!lexer.peek().isIdentifier()) {
            throwExpected("identifier");
        }

        return lexer.next().value;
    }

    LongName parseLongName () throws IOException {
        if (lexer.peek().isIdentifier()) {
            ArrayList<String> names = new ArrayList();
            names.add(lexer.next().value);

            while (lexer.peek().isSymbol(".") && lexer.peek(1).isIdentifier()) {
                lexer.skip();
                names.add(lexer.next().value);
            }

            return new LongName(names.toArray(new String[]{}));
        }
        return null;
    }

    Modifier parseModifier () throws IOException {
        Modifier mod = new Modifier();

        while (true) {
            if (tryConsumeKeyword("private")) {
                mod.visibility = Modifier.Visibility.PRIVATE;
                continue;
            } else if (tryConsumeKeyword("public")) {
                mod.visibility = Modifier.Visibility.PUBLIC;
                continue;
            } else if (tryConsumeKeyword("protected")) {
                mod.visibility = Modifier.Visibility.PROTECTED;
                continue;
            }

            if (tryConsumeKeyword("abstract")) {
                mod._abstract = true;
                continue;
            } else if (tryConsumeKeyword("final")) {
                mod._final = true;
                continue;
            } else if (tryConsumeKeyword("static")) {
                mod._static = true;
                continue;
            }

            break;
        }

        return mod;
    }

    TypeNode parseType () throws IOException, ParseException {
        LongName ident = parseLongName();
        if (ident == null) return null;

        TypeNode type = new BaseTypeNode(ident);

        while (true) {
            Token peek = lexer.peek();

            if (peek.isSymbol("<")) {
                throw new Error("Generics not yet implemented");
            } else if (peek.isSymbol("[") && lexer.peek(1).isSymbol("]")) {
                // Consume twice
                lexer.next();
                lexer.next();

                type = new ArrayTypeNode(type);
            } else {
                return type;
            }
        }
    }

    void throwExpected (String name) throws ParseException {
        throw expected(name);
    }

    ParseException expected (String name) {
        return new ParseException("Expected " + name + ", found " + lexer.peek());
    }

    Error unimplemented () {
        return new Error("unimplemented");
    }

    Error unimplemented (String feature) {
        return new Error("unimplemented: " + feature);
    }
}