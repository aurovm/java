package org.auro.java.parser;

import java.util.ArrayList;
import org.auro.java.Token;
import org.auro.java.Lexer;
import org.auro.java.node.*;

public class TypeDeclarationParser extends Parser {
    public TypeDeclarationParser (Lexer lx) {
        super(lx);
    }

    TypeDeclaration parse () throws java.io.IOException, ParseException {
        if (tryConsumeKeyword("class")) {
            ClassParser parser = new ClassParser(lexer);
            ClassDeclaration cl = parser.parseWithoutKeyword();
            return cl;
        } else if (tryConsumeKeyword("enum")) {
            throw new Error("enum not yet supported");
        } else if (tryConsumeKeyword("interface")) {
            throw new Error("interface not yet supported");
        } else {
            return null;
        }
    }
}