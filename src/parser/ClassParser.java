package org.auro.java.parser;

import java.util.ArrayList;
import java.io.IOException;
import org.auro.java.Token;
import org.auro.java.Lexer;
import org.auro.java.node.*;

public class ClassParser extends ExpressionParser {
    ClassDeclaration cl = new ClassDeclaration();

    TypeDeclarationParser typeDelcarationParser;
    MethodDeclarationParser methodDeclarationParser;

    ArrayList<TypeDeclaration> typeDeclarations = new ArrayList();
    ArrayList<FieldDeclaration> fieldDeclarations = new ArrayList();
    ArrayList<MethodDeclaration> methodDeclarations = new ArrayList();
    ArrayList<MethodDeclaration> constructors = new ArrayList();

    public ClassParser (Lexer lx) {
        super(lx);

        typeDelcarationParser = new TypeDeclarationParser(lx);
        methodDeclarationParser = new MethodDeclarationParser(lx);
    }

    // Parses a class without consuming the class keyword (it's assumed it was already consumed from the lexer)
    public ClassDeclaration parseWithoutKeyword () throws IOException, ParseException {
        if (!lexer.peek().isIdentifier()) {
            throwExpected("identifier");
        }

        cl.name = lexer.next().value;

        parseTypeParameters();

        if (tryConsumeKeyword("extends")) {
            cl.superclass = parseLongName();
            // Just to make it crash
            parseTypeParameters();
        }

        if (tryConsumeSymbol("inherits")) {
            throw new Error("Interfaces not yet implemented");
        }

        expectSymbol("{");

        while (!tryConsumeSymbol("}")) {
            parseMember();
        }

        cl.types = typeDeclarations.toArray(new TypeDeclaration[]{});
        cl.fields = fieldDeclarations.toArray(new FieldDeclaration[]{});
        cl.methods = methodDeclarations.toArray(new MethodDeclaration[]{});
        cl.constructors = constructors.toArray(new MethodDeclaration[]{});

        return cl;
    }

    void parseTypeParameters () throws IOException, ParseException {
        if (tryConsumeSymbol("<")) {
            throw new Error("Type parameters not yet implemented");
        }
    }

    void parseMember () throws IOException, ParseException {
        Modifier modifier = parseModifier();

        TypeDeclaration typeDecl = typeDelcarationParser.parse();
        if (typeDecl != null) {
            typeDecl.modifier = modifier;
            typeDeclarations.add(typeDecl);
            return;
        }

        if (tryConsumeKeyword("void")) {
            MethodDeclaration method = methodDeclarationParser.parseAfterVoid();
            method.modifier = modifier;

            methodDeclarations.add(method);
            return;
        }

        TypeNode type = parseType();

        if (type == null) {
            throwExpected("type declaration or type");
        }

        if (tryConsumeSymbol("(")) {
            MethodDeclaration method = methodDeclarationParser.parseAfterParen(type, null);
            method.modifier = modifier;

            constructors.add(method);
            return;
        }

        if (!lexer.peek().isIdentifier()) {
            throwExpected("identifier");
        }

        String name = lexer.next().value;

        if (tryConsumeSymbol(";")) {
            FieldDeclaration field = new FieldDeclaration();
            field.modifier = modifier;
            field.type = type;
            field.name = name;
            field.value = null;

            fieldDeclarations.add(field);
        } else if (tryConsumeSymbol("=")) {
            FieldDeclaration field = new FieldDeclaration();
            field.modifier = modifier;
            field.type = type;
            field.name = name;

            field.value = parseExpression();

            expectSymbol(";");

            fieldDeclarations.add(field);
        } else if (tryConsumeSymbol("(")) {
            MethodDeclaration method = methodDeclarationParser.parseAfterParen(type, name);
            method.modifier = modifier;

            methodDeclarations.add(method);
        } else {
            throwExpected("';', '=' or '('");
        }
    }
}