package org.auro.java.parser;

import java.util.ArrayList;
import java.io.IOException;
import org.auro.java.Token;
import org.auro.java.Lexer;
import org.auro.java.node.*;

public class StatementParser extends ExpressionParser {

    public StatementParser (Lexer lx) {
        super(lx);
    }

    StatementNode parseStatement () throws IOException, ParseException {
        return null;
    }

    BlockStatement parseBlock () throws IOException, ParseException {
        if (!tryConsumeSymbol("{")) return null;

        ArrayList<StatementNode> stmts = new ArrayList();

        while (!tryConsumeSymbol("}")) {
            Expression expr = parseExpression();

            if (expr != null) {
                if (tryConsumeSymbol("=")) {
                    if (expr.isAsignable()) {
                        throw unimplemented();
                    } else {
                        throw new ParseException("Expression is not assignable");
                    }
                } else if (lexer.peek().isIdentifier()) {
                    TypeNode tp = expr.toType();
                    if (tp == null) {
                        throw new ParseException("Expression is not a type: " + expr);
                    }

                    String name = expectIdent();

                    Expression val = null;
                    if (tryConsumeSymbol("=")) {
                        val = parseExpression();
                    }

                    stmts.add(new VarDeclaration(tp, name, val));
                } else {
                    // TODO: Check if the expression is a valid statement (not all expressions are)
                    stmts.add(new ExpressionStatement(expr));
                }

                expectSymbol(";");

            } else {
                StatementNode stmt = parseStatement();
                if (stmt == null) {
                    throwExpected("statement");
                }
                stmts.add(stmt);
            }

        }

        BlockStatement block = new BlockStatement();
        block.statements = stmts.toArray(new StatementNode[]{});
        return block;
    }
}