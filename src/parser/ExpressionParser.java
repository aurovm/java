package org.auro.java.parser;

import java.util.ArrayList;
import java.io.IOException;
import org.auro.java.Token;
import org.auro.java.Lexer;
import org.auro.java.node.*;

// Reference: jdk18/src/jdk.compiler/share/classes/com/sun/tools/javac/parser/JavacParser.java

public class ExpressionParser extends Parser {

    public ExpressionParser (Lexer lx) {
        super(lx);
    }

    Expression term0 () throws IOException, ParseException  {
        Expression expr = term1();
        if (expr == null) return null;

        if (tryConsumeSymbol("=")) {
            Expression right = term1();
            if (right == null) {
                throw expected("expression");
            }

            return new AssignmentExpression(expr, right);
        } else {
            return expr;
        }
    }

    Expression term1 () throws IOException, ParseException {
        Expression expr = term2();
        // TODO: Ternary
        return expr;
    }

    Expression term2 () throws IOException, ParseException {
        Expression expr = term3();
        // TODO: Ternary
        return expr;
    }

    Expression term3 () throws IOException, ParseException {
        /*
        line 1148

        case QUES:
        case PLUSPLUS: case SUBSUB: case BANG: case TILDE: case PLUS: case SUB:
        case LPAREN:
        case THIS:
        case SUPER:

        case INTLITERAL: case LONGLITERAL: case FLOATLITERAL: case DOUBLELITERAL:
        case CHARLITERAL: case STRINGLITERAL:
        case TRUE: case FALSE: case NULL:

        case NEW:
        case MONKEYS_AT:
        case UNDERSCORE: case IDENTIFIER: case ASSERT: case ENUM:

        case BYTE: case SHORT: case CHAR: case INT: case LONG: case FLOAT:
        case DOUBLE: case BOOLEAN:

        case VOID:
        
        */

        Token peek = lexer.peek();

        // LITERALS
        if (peek.type == Token.Type.NUMBER) {
            return new IntLiteral((int) Float.parseFloat(lexer.next().value));
        } else if (peek.type == Token.Type.STRING) {
            return new StringLiteral(lexer.next().value);

        } else if (tryConsumeKeyword("new")) {
            TypeNode type = parseType();
            if (lexer.peek().isSymbol("(")) {
                return new NewExpression(type, arguments());
            } else {
                throw expected("constructor arguments");
            }

        } else if (peek.isIdentifier()) {
            String name = lexer.next().value;
            Expression t = new Identifier(new LongName(new String[]{name}));

            loop:
            while (true) {
                if (tryConsumeSymbol("[")) {
                    throw unimplemented("Bracket expressions");
                } else if (lexer.peek().isSymbol("(")) {
                    t = new ApplyExpression(t, arguments());
                } else if (tryConsumeSymbol(".")) {
                    t = new SelectExpression(t, expectIdent());
                } else {
                    // missing cases ELLIPSIS and LT
                    break loop;
                }
            }

            return t;
        } else {
            throw unimplemented("Non Identifier Expression" + peek.toString());
        }

        // TODO: Ternary
        //return expr;
    }

    Expression[] arguments () throws IOException, ParseException {
        expectSymbol("(");

        ArrayList<Expression> args = new ArrayList();

        if (!tryConsumeSymbol(")")) {
            args.add(parseExpression());

            while (tryConsumeSymbol(",")) {
                args.add(parseExpression());
            }

            expectSymbol(")");
        }

        return args.toArray(new Expression[]{});
    }

    Expression parseExpression () throws IOException, ParseException {
        return term0();
    }
}