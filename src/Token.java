package org.auro.java;

import java.util.HashSet;
import java.util.Arrays;

public class Token {
    public enum Type {
        KEYWORD, STRING, NUMBER, IDENTIFIER, SYMBOL,
    }

    public final Type type;
    public final String value;

    private final static HashSet<String> KEYWORD_SET = new HashSet(
        Arrays.asList(
            new String[]{
                "package", "import", "class", "interface", "enum",
                "public", "private", "protected",
                "static", "abstract", "final",
                "extends", "implements",
                "if", "else", "for", "while", "case", "default", "return",
                "null", "true", "false",
                "void", "new", "instanceof",
            }
        )
    );

    Token (Type type) {
        this(type, null);
    }

    Token (Type type, String value) {
        this.type = type;
        this.value = value;
    }

    public String toString () {
        if (this.value == null) {
            return this.type.toString();
        } else {
            return this.type.toString() + '(' + this.value + ')';
        }
    }

    public Token maybePromoteKeyword () {
        if (KEYWORD_SET.contains(this.value)) {
            return new Token(Type.KEYWORD, this.value);
        } else {
            return this;
        }
    }

    public boolean isIdentifier () { return this.type == Type.IDENTIFIER; }

    public boolean isKeyword (String kw) {
        return this.type == Type.KEYWORD && this.value.equals(kw);
    }

    public boolean isSymbol (String kw) {
        return this.type == Type.SYMBOL && this.value.equals(kw);
    }
}