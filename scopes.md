
# Scopes (concept)

~~~java
// Default one used for execution scopes
class Scope {
    Scope getMember ();
}

// Has the constants and imports
class GlobalScope extends Scope {}

// Scope for a class namespace
class ClassScope extends Scope {}

class ExecutionScope extends Scope {
    ArrayList<Scopes> extends;
    ArrayList<Scopes> members;

    void extend (Scope);
    void add (Scope);
}

// Scope for a class instance (or null) from a variable. It exposes the instance methods and fields
class InstanceScope extends Scope {}

// TODO: Auro Module Scopes
~~~
